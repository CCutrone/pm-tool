﻿using System;
using System.IO;

namespace PMTool.Control {
    /// <summary>
    /// A custom Exception to be thrown whenever a remote computer cannot be found
    /// </summary>
    [Serializable]
    public class ComputerNotFoundException : IOException {
        string computerName;
        public ComputerNotFoundException(string computerName): base($"Computer ({computerName}) was not found") {
            this.computerName = computerName;
        }
    }
}
