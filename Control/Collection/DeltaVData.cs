﻿using System.Collections.Generic;

namespace PMTool.Control.Collection {
    class DeltaVData {
        public readonly string type, autoLogon, autoLaunch, autoSwitch, dvDataLocation, gdi, user;
        public readonly Dictionary<string, string> operateSettings;
        internal DeltaVData(string type, string autoLogon, string autoLaunch, string autoSwitch, string dvDataLocation, string gdi, string user, Dictionary<string, string> operateSettings) {
            this.type = type; //workstation type
            this.autoLogon = autoLogon; //auto logon configuration
            this.autoLaunch = autoLaunch; //auto launch DV Operate
            this.autoSwitch = autoSwitch; //auto switch to DV desktop
            this.dvDataLocation = dvDataLocation; //default DVData location
            this.gdi = gdi;
            this.user = user;
            this.operateSettings = operateSettings;
        }
    }
}
