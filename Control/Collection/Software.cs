﻿using System;
using System.Collections.Generic;

namespace PMTool.Control.Collection {
    class Software {
        internal string name;
        internal string version;
        internal Software(string name, string date, string version) {
            this.name = name;
            this.version = version;
        }
        public override bool Equals(Object obj) {
            if (obj == null || !GetType().Equals(obj.GetType())) return false;
            Software sw = (Software) obj;
            return name == sw.name && version == sw.version;
        }

        public override int GetHashCode() {
            return name.GetHashCode() ^ (version ?? "").GetHashCode();
        }
        public override string ToString() {
            return $"{name}, {version}";
        }

        public readonly static List<string> BLACKLIST = new List<string>() {
            "AMD",
            "Browser for SQL Server",
            "Catalyst Control Center ", //we want CCC, just not all of it's children. Hence the space at the end of it
            "CCC Help ",
            "ccc-utility",
            "Dragon",
            "Intel",
            "Matrox Graphics Software",
            "Microsoft",
            "NextGenBase",
            "Objectivity/DB",
            "OpenSLP",
            "Pk ",
            "Platform Communications PC",
            "Realtek",
            "Sentinel Protection",
            "SmartCommissioning",
            "SQL Server Browser",
            "Sql Server Customer Experience Improvement Program",
            "Trex Sync",
            "Visual Basic for Applications",
            "vs_",
            "Vulkan",
            "Windows SDK"
        };
    }
}
