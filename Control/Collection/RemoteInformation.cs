﻿using Microsoft.Win32;
using PMTool.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Text.RegularExpressions;

namespace PMTool.Control.Collection {
    static class RemoteInformation {

        private static readonly ConnectionOptions connectionOptions = new ConnectionOptions();

        private static decimal ConvertBytesToGB(object driveSpace) => decimal.Round(Convert.ToDecimal(driveSpace) / 1024 / 1024 / 1024, 2);
        /// <summary>
        /// Gets the current monitor layout/configuration for each monitor connected to the workstation. This includes the monitor index, position, and resolution
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with each monitor layout/configuration</param>
        internal static void GetMonitorLayout(string computerName, Action<Monitor> action) {
            long timestamp = 0;
            long tempTimestamp = 0;
            List<Monitor> monitors = new List<Monitor>();
            GetRegistrySubkeysObjects(computerName, @"SYSTEM\CurrentControlSet\Control\GraphicsDrivers\Configuration", (subKey) => {
                tempTimestamp = long.Parse(subKey.GetValue("Timestamp").ToString());
                if(tempTimestamp > timestamp) {
                    timestamp = tempTimestamp;
                    monitors.Clear();
                    string[] monitorIndexes = subKey.GetSubKeyNames();
                    foreach(string index in monitorIndexes) {
                        RegistryKey monitor = subKey.OpenSubKey(index);
                        monitors.Add(
                            new Monitor(index, 
                                        monitor.GetValue("Position.cx").ToString(), 
                                        monitor.GetValue("Position.cy").ToString(),
                                        monitor.GetValue("PrimSurfSize.cx").ToString(),
                                        monitor.GetValue("PrimSurfSize.cy").ToString()
                            )
                        );
                    }
                }
            });
            foreach(Monitor monitor in monitors) {
                action(monitor);
            }
        }

        internal static void GetFileContent(string filename, Action<string[]> action) {
            if (!File.Exists(filename)) {
                Logger.Error($"Could not find file: {filename}");
                return;
            }
            action(File.ReadAllLines(filename));
        }

        /// <summary>
        /// Gets the DeltaV information from the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with the DeltaVData object</param>
        internal static void GetDeltaVInformation(string computerName, Action<DeltaVData> action) {
            string gdi = "";
            string user = "";
            string type = "";
            string autoLogon = "";
            string autoLaunch = "";
            string autoSwitch = "";
            string dvDataLocation = "";
            Dictionary<string, string> values = new Dictionary<string, string>();

            GetRegistryValueObjects(computerName, @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows\", (subKey) => {
                if (subKey.GetValue("GDIProcessHandleQuota") is int tempGDI) gdi = "" + tempGDI;
                if (subKey.GetValue("USERProcessHandleQuota") is int tempUser) user = "" + tempUser;
            });
            GetRegistryValueObjects(computerName, new string[] { @"SOFTWARE\Wow6432Node\FRSI\DeltaV\CurrentVersion", @"SOFTWARE\FRSI\DeltaV\CurrentVersion" }, (subKey) => {
                if (subKey.GetValue("WSMode") is object tempType && tempType.ToString().Trim() != "") type = tempType.ToString().Trim();
                if (subKey.GetValue("AutoDvLogon") is int tempAutoLogon) autoLogon = tempAutoLogon == 1 ? "True" : "False";
                if (subKey.GetValue("AutoLaunchOperatorInterface") is int tempAutoLaunch) autoLaunch = tempAutoLaunch == 1 ? "True" : "False";
                if (subKey.GetValue("AutoSwitchingEnabled") is int tempAutoSwitch) autoSwitch = tempAutoSwitch == 1 ? "True" : "False";
                if (subKey.GetValue("DataPath") is string tempDvDataLocation && tempDvDataLocation.Trim() != "") dvDataLocation = tempDvDataLocation;
            });

            GetFileContent($@"{dvDataLocation}\Graphics-iFix\Local\FixUserPreferences.ini", (content) => {
                foreach(string line in content){
                    if (!line.Contains("=")) continue;

                    string[] keyValue = line.Split('=');
                    values.Add(keyValue[0], keyValue[1]);
                }
            });

            action(new DeltaVData(type, autoLogon, autoLaunch, autoSwitch, dvDataLocation, gdi, user, values));
        }
        /// <summary>
        /// Gets the Application Event Logs from the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with the event log entries</param>
        internal static void GetApplicationEventLogs(string computerName, Action<string, string, string, string> action) {
            string date = ManagementDateTimeConverter.ToDmtfDateTime(DateTime.Now.AddDays(-30));
            string currentDate = "";

            DateTime currentDateTime = DateTime.Now;
            GetWMI(computerName, $"SELECT * FROM Win32_NTLogEvent WHERE Logfile='Application' AND EventType=1 AND TimeGenerated >='{date}'", (obj) => {
                if (currentDate != obj["TimeGenerated"] as string) {
                    currentDate = obj["TimeGenerated"] as string;
                    currentDateTime = ManagementDateTimeConverter.ToDateTime(currentDate);
                }
                action(obj["SourceName"] as string, currentDateTime.ToString(), $"{obj["EventCode"]}", obj["Message"] as string);
            });
        }
        /// <summary>
        /// Gets the System Event Logs from the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with the event log entries</param>
        internal static void GetSystemEventLogs(string computerName, Action<string, string, string, string> action) {
            string date = ManagementDateTimeConverter.ToDmtfDateTime(DateTime.Now.AddDays(-30));
            string currentDate = "";
            DateTime currentDateTime = DateTime.Now;
            GetWMI(computerName, $"SELECT * FROM Win32_NTLogEvent WHERE Logfile='System' AND EventType=1 AND TimeGenerated >='{date}'", (obj) => {
                if (currentDate != obj["TimeGenerated"] as string) {
                    currentDate = obj["TimeGenerated"] as string;
                    currentDateTime = ManagementDateTimeConverter.ToDateTime(currentDate);
                }
                action(obj["SourceName"] as string, currentDateTime.ToString(), $"{obj["EventCode"]}", obj["Message"] as string);
            });
        }
        /// <summary>
        /// Gets the activation status of the os of the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with the os activation status</param>
        internal static void GetWindowsActivation(string computerName, Action<int> action) {
            GetWMI(computerName, "SELECT LicenseStatus FROM SoftwareLicensingProduct WHERE PartialProductKey <> null AND ApplicationId='55c92734-d682-4d71-983e-d6ec3f16059f' AND LicenseIsAddon=False", (result) => {
                action(int.Parse(result["LicenseStatus"].ToString()));
            });
        }
        /// <summary>
        /// Gets the os of the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with the os name</param>
        internal static void GetOperatingSystemInformation(string computerName, Action<string> action) {
            GetWMI(computerName, "SELECT Caption FROM Win32_OperatingSystem", (result) => {
                action(result["Caption"] as string);
            });
        }
        /// <summary>
        /// Gets the system information of the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with the system information</param>
        internal static void GetModelInformation(string computerName, Action<string, string> action) {
            GetWMI(computerName, "SELECT Domain, Model FROM Win32_ComputerSystem", (obj) => {
                action(obj["Domain"] as string, obj["Model"] as string);
            });
        }

        /// <summary>
        /// Gets the BIOS version of the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with the bios version</param>
        internal static void GetBIOSInformation(string computerName, Action<string> action) {
            GetWMI(computerName, "SELECT Manufacturer, Name FROM Win32_Bios", (obj) => {
                action($"{obj["Manufacturer"]} {obj["Name"]}");
            });
        }

        /// <summary>
        /// Gets the amount of physical memory of the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">The action to be performed with the amount of physical memory</param>
        internal static void GetMemoryInformation(string computerName, Action<string> action) {
            long bytesOfMemory = 0;
            GetWMI(computerName, "SELECT Capacity FROM Win32_PhysicalMemory", (obj) => {
                bytesOfMemory += long.Parse($"{obj["Capacity"]}");
            });
            action($"{ConvertBytesToGB(bytesOfMemory)}");
        }            
        /// <summary>
        /// Symantec Antivirus definition versions are determined by a folder path which is then put into an XML node
        /// </summary>
        /// <param name="directoryPath">The path of the Symantec definition location (based on Symantec Version)</param>
        /// <returns>The AV definition version number</returns>
        private static string ConvertFolderToSymantecVersion(string directoryPath) {
            string[] directories = Directory.GetDirectories(directoryPath);
            
            string symantecVersion = "";
            for(int i = 0; i < directories.Length && symantecVersion == ""; i++) {
                symantecVersion = directories[i].Split('\\').Last();
                if (!Regex.Match(symantecVersion, "^[0-9].[0-9]").Success) symantecVersion = "";
            }

            return symantecVersion.Split('.').First();
        }
        /// <summary>
        /// Gets the Antivirus definition details for either Symantec or McAfee and puts them into the XML document
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with the software name and version number</param>
        internal static void GetLatestAntivirusDefinition(string computerName, Action<string, string> action) {
            string symantecDefinitionLocation14 = @"C:\ProgramData\Symantec\Symantec Endpoint Protection\CurrentVersion\Data\Definitions\VirusDefs\";
            string symantecDefinitionLocation12 = @"C:\ProgramData\Symantec\Symantec Endpoint Protection\CurrentVersion\Data\Definitions\SDSDefs\";
            string[] mcafeeDefinitionLocations = new string[] { 
                @"SOFTWARE\Wow6432Node\McAfee\Agent",
                @"SOFTWARE\McAfee\Agent"
            };
            if (Directory.Exists(symantecDefinitionLocation14))
                action("Symantec v14", ConvertFolderToSymantecVersion(symantecDefinitionLocation14));
            else if (Directory.Exists(symantecDefinitionLocation12))
                action("Symantec v12", ConvertFolderToSymantecVersion(symantecDefinitionLocation12));
            else
                GetRegistryValueObjects(computerName, mcafeeDefinitionLocations, (obj) => {
                    if(obj.GetValue("AgentVersion") != null)
                        action("McAfee", ($"{obj.GetValue("AgentVersion")}"));
                });
        }
        /// <summary>
        /// Gets the list of installed Software on the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with the Name, Size, and the FreeSpace values</param>
        internal static void GetDriveSpaceInBytes(string computerName, Action<string, string, string> action) {
            GetWMI(computerName, "SELECT Size, FreeSpace, Name, FileSystem FROM Win32_LogicalDisk", (obj) => {
                action(obj["Name"] as string, $"{ConvertBytesToGB(obj["Size"])}", $"{ConvertBytesToGB(obj["FreeSpace"])}");
            });
        }
        /// <summary>
        /// Gets the disk status on the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with the model and disk status</param>
        internal static void GetDiskStatus(string computerName, Action<string, string> action) {
            GetWMI(computerName, "SELECT Status, Model FROM Win32_DiskDrive", (obj) => {
                action(obj["Model"] as string, obj["Status"] as string);
            });
        }
        /// <summary>
        /// Gets the color depth of the monitor (assuming that one exists)
        /// </summary>
        /// <param name="computerName">The name of the computer</param>
        /// <param name="action">Action to be performed with the name and bits per pixel of each monitor</param>
        internal static void GetMonitorDepthOfColor(string computerName, Action<string, string> action) {
            GetWMI(computerName, "SELECT Name, CurrentBitsPerPixel FROM Win32_VideoController", (obj) => {
                action(obj["Name"] as string, $"{obj["CurrentBitsPerPixel"]}");
            });
        }
        /// <summary>
        /// Combines two different WMI objects to get a full picture of the NICs on the station.
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with each NIC object</param>
        internal static void GetNICInformation(string computerName, Action<NIC> action) {
            Dictionary<int, NIC> nics = new Dictionary<int, NIC>();
            GetWMI(computerName, "SELECT * FROM Win32_NetworkAdapterConfiguration", (obj) => {
                int index = int.Parse($"{obj["InterfaceIndex"]}");
                nics[index] = new NIC($"{obj["IPConnectionMetric"] ?? "-1"}", "", "", obj["DefaultIPGateway"] as string[], obj["IPSubnet"] as string[], obj["IPAddress"] as string[], obj["DNSServerSearchOrder"] as string[]);
            });
            GetWMI(computerName, "SELECT * FROM Win32_NetworkAdapter", (obj) => {
                if(obj["InterfaceIndex"] != null) {
                    int index = int.Parse($"{obj["InterfaceIndex"]}");
                    NIC nic = nics[index];
                    nic.name = (obj["NetConnectionID"] as string) ?? "";
                    nic.SetStatus($"{obj["NetConnectionStatus"]}");
                }
            });
            foreach(NIC nic in nics.Values.OrderBy(it => it.interfaceIndex)) {
                action(nic);
            }
        }
        /// <summary>
        /// Gets the service tag on the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with the SerialNumber value</param>
        internal static void GetServiceTag(string computerName, Action<string> action) {
            GetWMI(computerName, "SELECT SerialNumber FROM Win32_SystemEnclosure", (obj) => {
                action(obj["SerialNumber"] as string);
            });
        }
        /// <summary>
        /// Gets the timezone on the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with the Caption value</param>
        internal static void GetTimezone(string computerName, Action<string> action) {
            GetWMI(computerName, "SELECT Caption FROM Win32_TimeZone", (obj) => {
                action(obj["Caption"] as string);
            });
        }
        /// <summary>
        /// Gets the video driver information on the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with the Name and the DriverVersion values</param>
        internal static void GetVideoDrivers(string computerName, Action<string, string> action) {
            GetWMI(computerName, "SELECT Name, DriverVersion FROM Win32_VideoController", (obj) => {
                action(obj["Name"] as string, obj["DriverVersion"] as string);
            });
        }
        /// <summary>
        /// Gets the list of installed Microsoft Patches on the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with the HotfixID and the InstalledOn values</param>
        internal static void GetMostRecentWindowsUpdates(string computerName, Action<string, string> action) {
            DateTime date = DateTime.Parse("1/1/1991");
            CultureInfo info = new CultureInfo("en-US");
            string hotfixID = "";

            GetWMI(computerName, "SELECT HotfixID, InstalledOn FROM Win32_QuickFixEngineering", (obj) => {
                DateTime temp = DateTime.Parse("1/1/1991");
                try {
                     temp = DateTime.Parse(obj["InstalledOn"] as string, info);
                } catch (Exception _) { } // not every entry has an installedon key/value, which will throw an error that can be ignored
                
                if (temp > date) {
                    date = temp;
                    hotfixID = obj["HotfixID"] as string;
                }
            });
            action(hotfixID, date.ToString());
        }

        /// <summary>
        /// Gets the list of installed Software on the remote computer
        /// </summary>
        /// <param name="computerName">The computer name</param>
        /// <param name="action">Action to be performed with the DisplayName, DisplayVersion, and the InstalledOn values</param>
        internal static void GetInstalledSoftware(string computerName, Action<string, string, string> action) {
            HashSet<Software> installedSoftware = new HashSet<Software> ();
            GetRegistrySubkeysObjects(
                computerName,
                new string[] {
                    @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall",
                    @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                },
                (subKey) => {
                    if (!(subKey.GetValue("DisplayName") is string name) || name.Trim('\0') == "" || Software.BLACKLIST.Any((software) => name.ToLower().Contains(software.ToLower()))) return;
                    
                    string version = subKey.GetValue("DisplayVersion") as string;
                    string installedOn = subKey.GetValue("InstallDate") as string;
                    installedSoftware.Add(new Software(name.Trim('\0').Trim(), installedOn, version));
                });

            installedSoftware
                .OrderBy((a) => a.name)
                .ToList()
                .ForEach((software) => action(software.name, software.version, ""));
        }
        /// <summary>
        /// Gets the registry key at the specified path and passes it the action method
        /// </summary>
        /// <param name="computerName">The name of the computer</param>
        /// <param name="registryLocation">The path of the registry key</param>
        /// <param name="action">The action that will use the registry subkey value</param>
        internal static void GetRegistrySubkeysObjects(string computerName, string registryLocation, Action<RegistryKey> action) {
            RegistryKey subKey;
            using (RegistryKey remoteRegistry = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName)) {
                using (RegistryKey registryKey = remoteRegistry.OpenSubKey(registryLocation)) {
                    if (registryKey == null) {
                        Logger.Warn($"Registry path: {registryKey} not found for computer {computerName}.");
                        return;
                    }
                    foreach (string keyname in registryKey.GetSubKeyNames()) {
                        using (subKey = registryKey.OpenSubKey(keyname)) {
                            action(subKey);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Gets the registry key at the specified path and passes it the action method
        /// </summary>
        /// <param name="computerName">The name of the computer</param>
        /// <param name="registryLocation">The path of the registry key</param>
        /// <param name="action">The action that will use the registry values</param>
        internal static void GetRegistryValueObjects(string computerName, string registryLocation, Action<RegistryKey> action) {
            using (RegistryKey remoteRegistry = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName)) {
                using (RegistryKey registryKey = remoteRegistry.OpenSubKey(registryLocation)) {
                    if (registryKey == null) {
                        Logger.Warn($"Registry path: {registryKey} not found for computer {computerName}.");
                        return;
                    }
                    action(registryKey);
                }
            }
        }
        /// <summary>
        /// Gets the registry key at the specified path and passes it the action method
        /// </summary>
        /// <param name="computerName">The name of the computer</param>
        /// <param name="registryLocation">The path of the registry key</param>
        /// <param name="action">The action that will use the registry subkey value</param>
        internal static void GetRegistrySubkeysObjects(string computerName, string[] registryLocations, Action<RegistryKey> action) {
            RegistryKey subKey;
            bool shouldContinue = true;
            using (RegistryKey remoteRegistry = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName)) {
                for (int i = 0; i < registryLocations.Length && shouldContinue; i++) {
                    using (RegistryKey registryKey = remoteRegistry.OpenSubKey(registryLocations[i])) {
                        if (registryKey == null) {
                            Logger.Warn($"Registry path: {registryLocations[i]} not found for computer {computerName}.");
                            continue;
                        } else {
                            Logger.Log($"Registry path: {registryLocations[i]} found found for computer {computerName}.");
                            shouldContinue = false;
                        }
                        foreach (string keyname in registryKey.GetSubKeyNames()) {
                            using (subKey = registryKey.OpenSubKey(keyname)) {
                                action(subKey);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the registry key at the specified path and passes it the action method
        /// </summary>
        /// <param name="computerName">The name of the computer</param>
        /// <param name="registryLocation">The path of the registry key</param>
        /// <param name="action">The action that will use the registry values</param>
        internal static void GetRegistryValueObjects(string computerName, string[] registryLocations, Action<RegistryKey> action) {
            bool shouldContinue = true;
            using (RegistryKey remoteRegistry = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName)) {
                for (int i = 0; i < registryLocations.Length && shouldContinue; i++) {
                    using (RegistryKey registryKey = remoteRegistry.OpenSubKey(registryLocations[i])) {
                        if (registryKey == null) {
                            Logger.Warn($"Registry path: {registryLocations[i]} not found for computer {computerName}.");
                            continue;
                        } else {
                            Logger.Log($"Registry path: {registryLocations[i]} found found for computer {computerName}. Keynames: \n----{string.Join("\n----", registryKey.GetValueNames())}");
                            shouldContinue = false;
                        }
                        action(registryKey);
                    }
                }
            }
        }
        /// <summary>
        /// Retrieves a WMI Object from the specified computer using the query provided. Each object in the query is then passed as an argument to the action method
        /// </summary>
        /// <param name="computerName">The name of the remote computer</param>
        /// <param name="query">The query to use to get the WMI Object</param>
        /// <param name="action">The action to perform on each object the query retrieves</param>
        internal static void GetWMI(string computerName, string query, Action<ManagementObject> action) {
            if (!Directory.Exists($@"\\{computerName}\{EnvironmentHelper.DRIVE}$"))
                throw new ComputerNotFoundException(computerName);

            ManagementScope scope = new ManagementScope($@"\\{computerName}\root\cimv2", connectionOptions);
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, new ObjectQuery(query));
            ManagementObjectCollection collection = searcher.Get();

            Logger.Log($"Querying {computerName} with query: \"{query}\"");

            foreach (ManagementObject obj in collection) action(obj);
        }
    }
}
