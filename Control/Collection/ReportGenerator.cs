﻿using PMTool.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Xml;

namespace PMTool.Control.Collection {
    class ReportGenerator {
        // Listens for what computer is currently having data collected
        readonly List<Action<string>> currentComputerListeners = new List<Action<string>>();
        // Listens for updates to what step is currently being performed
        internal readonly List<Action<string>> currentStepListeners = new List<Action<string>>();
        internal List<string> Generate() {
            List<string> computerNotFound = new List<string>();

            // creates the XML document to use for the report
            XmlDocument document = new XmlDocument();
            XmlNode xmlBody = document.CreateElement("doc");
            document.AppendChild(xmlBody);
            XmlNode xmlDate = document.CreateElement("date");
            xmlDate.InnerText = DateTime.Now.ToString();
            xmlBody.AppendChild(xmlDate);

            foreach (string computerName in EnvironmentHelper.Workstations) {
                try {
                    CollectData(computerName, document, xmlBody);
                } catch (ComputerNotFoundException e) {
                    Logger.Error(e.Message);
                    computerNotFound.Add(e.Message);
                }
            }
            XmlElement xmlNetwork = document.CreateElement("network");
            SetStep("Gathering Network Information");
            foreach (DeltaVNode node in EnvironmentHelper.DeltaVNodes) {
                XmlElement xmlDeltaVNode = document.CreateElement("node");
                xmlDeltaVNode.AppendChild(CreateElementWithText(document, "name", node.name));
                xmlDeltaVNode.AppendChild(CreateElementWithText(document, "type", node.type));
                xmlDeltaVNode.AppendChild(CreateElementWithText(document, "primary", node.primary));
                xmlDeltaVNode.AppendChild(CreateElementWithText(document, "secondary", node.secondary));
                xmlNetwork.AppendChild(xmlDeltaVNode);
            }
            xmlBody.AppendChild(xmlNetwork);
            // saves the data as an XML document 
            Directory.CreateDirectory($@"{EnvironmentHelper.DRIVE}:\ControlWorx\Reports\");
            document.Save($@"{EnvironmentHelper.DRIVE}:\ControlWorx\Reports\Data-{DateTime.Today.ToString("d").Replace("/", "-")}.xml");

            // creates a viewable xml document with a premade stylesheet
            XmlDocument viewable = new XmlDocument();
            Assembly asm = Assembly.GetExecutingAssembly();
            using (Stream stream = asm.GetManifestResourceStream("PMTool.ReportTemplate.xml")) {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ProhibitDtd = false;
                XmlReader reader = XmlReader.Create(stream, settings);
                viewable.Load(reader);
            }
            XmlNode content = document.GetElementsByTagName("doc")[0];
            viewable.GetElementsByTagName("doc")[0].InnerXml += content.InnerXml;
            viewable.Save($@"{EnvironmentHelper.DRIVE}:\ControlWorx\Reports\{DateTime.Today.ToString("d").Replace("/", "-")}.xml");
            Logger.Save();
            return computerNotFound; // returns a list of any computer that could not be reached
        }
        /// <summary>
        /// Collects data on all of the areas that are enabled (see CollectionSettings.cs)
        /// </summary>
        /// <param name="computerName">The computer to collect from</param>
        /// <param name="document">The XMLDocument</param>
        /// <param name="xmlCollection">The outer XMLNode</param>
        internal void CollectData(string computerName, XmlDocument document, XmlNode xmlCollection) {
            XmlElement xmlComputer = document.CreateElement("computer");

            currentComputerListeners.ForEach(it => it(computerName));
            if(CollectionSettings.SYSTEM_INFO) {
                SetStep($"Getting service tag for {computerName}");
                RemoteInformation.GetServiceTag(computerName, (serviceTag) => {
                    xmlComputer.AppendChild(CreateElementWithText(document, "serviceTag", serviceTag));
                });
                SetStep($"Getting timezone for {computerName}");
                RemoteInformation.GetTimezone(computerName, (timezone) => {
                    xmlComputer.AppendChild(CreateElementWithText(document, "timezone", timezone));
                });
                SetStep($"Getting operating system name for {computerName}");
                RemoteInformation.GetOperatingSystemInformation(computerName, (os) => {
                    xmlComputer.AppendChild(CreateElementWithText(document, "os", os));
                });
                SetStep($"Getting BIOS version for {computerName}");
                RemoteInformation.GetBIOSInformation(computerName, (bios) => {
                    xmlComputer.AppendChild(CreateElementWithText(document, "bios", bios));
                });
                SetStep($"Getting RAM for {computerName}");
                RemoteInformation.GetMemoryInformation(computerName, (memory) => {
                    xmlComputer.AppendChild(CreateElementWithText(document, "memory", $"{memory} GB"));
                });
                SetStep($"Getting domain name and model number for {computerName}");
                RemoteInformation.GetModelInformation(computerName, (domain, model) => {
                    xmlComputer.AppendChild(CreateElementWithText(document, "domain", domain));
                    xmlComputer.AppendChild(CreateElementWithText(document, "model", model));
                });
                SetStep($"Getting windows activation status for {computerName}");
                RemoteInformation.GetWindowsActivation(computerName, (status) => {
                    xmlComputer.AppendChild(CreateElementWithText(document, "activationStatus", status == 1 ? "Activated" : "Unactivated"));
                });
                SetStep($"Getting DeltaV information for {computerName}");
                XmlElement xmlDeltaVInformation = document.CreateElement("deltav");
                RemoteInformation.GetDeltaVInformation(computerName, (deltavData) => {
                    xmlDeltaVInformation.AppendChild(CreateElementWithText(document, "station", deltavData.type));
                    xmlDeltaVInformation.AppendChild(CreateElementWithText(document, "autoLogon", deltavData.autoLogon));
                    xmlDeltaVInformation.AppendChild(CreateElementWithText(document, "autoLaunch", deltavData.autoLaunch));
                    xmlDeltaVInformation.AppendChild(CreateElementWithText(document, "autoSwitch", deltavData.autoSwitch));
                    xmlDeltaVInformation.AppendChild(CreateElementWithText(document, "dvDataLocation", deltavData.dvDataLocation));
                    xmlDeltaVInformation.AppendChild(CreateElementWithText(document, "gdi", deltavData.gdi));
                    xmlDeltaVInformation.AppendChild(CreateElementWithText(document, "user", deltavData.user));
                    XmlElement xmlOperateSettings = document.CreateElement("operateSettings");
                    deltavData.operateSettings.ToList().ForEach(kvp => xmlOperateSettings.AppendChild(CreateElementWithText(document, kvp.Key, kvp.Value)));
                    xmlDeltaVInformation.AppendChild(xmlOperateSettings);
                });
                xmlComputer.AppendChild(xmlDeltaVInformation);
            }
            if (CollectionSettings.DRIVE_SPACE) {
                SetStep($"Getting drive space for {computerName}");
                XmlElement xmlDriveDatagroup = document.CreateElement("drives");
                RemoteInformation.GetDriveSpaceInBytes(computerName, (drive, space, freespace) => {
                    XmlElement xmlDrive = document.CreateElement("drive");
                    xmlDrive.AppendChild(CreateElementWithText(document, "name", drive));
                    xmlDrive.AppendChild(CreateElementWithText(document, "space", $"{space} GB"));
                    xmlDrive.AppendChild(CreateElementWithText(document, "freeSpace", $"{freespace} GB"));
                    xmlDriveDatagroup.AppendChild(xmlDrive);
                });
                SetStep($"Getting disk status for {computerName}");
                XmlElement xmlDiskDatagroup = document.CreateElement("disks");
                RemoteInformation.GetDiskStatus(computerName, (model, status) => {
                    XmlElement xmlDisk = document.CreateElement("disk");
                    xmlDisk.AppendChild(CreateElementWithText(document, "model", model));
                    xmlDisk.AppendChild(CreateElementWithText(document, "status", status));
                    xmlDiskDatagroup.AppendChild(xmlDisk);
                });
                xmlComputer.AppendChild(xmlDriveDatagroup);
                xmlComputer.AppendChild(xmlDiskDatagroup);
            }
            if (CollectionSettings.NICS) {
                SetStep($"Getting NIC configuration for {computerName}");
                XmlElement xmlNICDatagroup = document.CreateElement("nics");
                RemoteInformation.GetNICInformation(computerName, (nic) => {
                    if (nic.name == "") return;

                    XmlElement xmlNIC = document.CreateElement("nic");
                    xmlNIC.AppendChild(CreateElementWithText(document, "interfaceIndex", $"{nic.interfaceIndex}"));
                    xmlNIC.AppendChild(CreateElementWithText(document, "name", nic.name));
                    xmlNIC.AppendChild(CreateElementWithText(document, "status", nic.status));
                    // not every NIC has a subnet set, this only adds the subnet element if one has been set
                    if (nic.subnet.Trim() != "") {
                        xmlNIC.AppendChild(CreateElementWithText(document, "subnet", nic.subnet));
                    }
                    // not every NIC has gateways, this only adds the gateways element if one one more existsZ
                    if (nic.gateways.Length > 0) {
                        XmlElement xmlGateways = document.CreateElement("gateways");
                        foreach (string gateway in nic.gateways) {
                            xmlGateways.AppendChild(CreateElementWithText(document, "gateway", gateway));
                        }
                        xmlNIC.AppendChild(xmlGateways);
                    }
                    // not every NIC has a configured IP, this only adds the ipAddresses element if one one more is manually set
                    if (nic.ips.Length > 0) {
                        XmlElement xmlIPAddresses = document.CreateElement("ipAddresses");
                        foreach (string ip in nic.ips) {
                            xmlIPAddresses.AppendChild(CreateElementWithText(document, "ip", ip));
                        }
                        xmlNIC.AppendChild(xmlIPAddresses);
                    }
                    // not every NIC has DNS settings configured, this only adds the dnsAddresses element if one one more is manually set
                    if (nic.dns.Length > 0) {
                        XmlElement xmlDNS = document.CreateElement("dnsAddresses");
                        foreach (string dns in nic.dns) {
                            xmlDNS.AppendChild(CreateElementWithText(document, "dns", dns));
                        }
                        xmlNIC.AppendChild(xmlDNS);
                    }
                    xmlNICDatagroup.AppendChild(xmlNIC);
                });
                xmlComputer.AppendChild(xmlNICDatagroup);
            }

            bool isAdmin = false;

            using (WindowsIdentity identity = WindowsIdentity.GetCurrent()) {
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            if (CollectionSettings.MONITORS) {
                if (isAdmin) {
                    SetStep($"Getting monitor layouts for {computerName}");
                    XmlElement xmlMonitorDatagroup = document.CreateElement("monitors");
                    RemoteInformation.GetMonitorLayout(computerName, (monitor) => {
                        XmlElement xmlMonitor = document.CreateElement("monitor");
                        xmlMonitor.AppendChild(CreateElementWithText(document, "index", monitor.index));
                        xmlMonitor.AppendChild(CreateElementWithText(document, "posX", monitor.posX));
                        xmlMonitor.AppendChild(CreateElementWithText(document, "posY", monitor.posY));
                        xmlMonitor.AppendChild(CreateElementWithText(document, "width", monitor.width));
                        xmlMonitor.AppendChild(CreateElementWithText(document, "height", monitor.height));
                        xmlMonitorDatagroup.AppendChild(xmlMonitor);
                    });
                    xmlComputer.AppendChild(xmlMonitorDatagroup);
                } else {
                    Logger.Error($"Attempted to get monitor layout for {computerName}, but lacked Administrator rights.");
                }
            } 
            if (CollectionSettings.VIDEO_DRIVERS) {
                SetStep($"Getting video driver version for {computerName}");
                XmlElement xmlVideoDriversDatagroup = document.CreateElement("videoDrivers");
                RemoteInformation.GetVideoDrivers(computerName, (name, version) => {
                    XmlElement xmlVideoDriver = document.CreateElement("driver");
                    xmlVideoDriver.AppendChild(CreateElementWithText(document, "name", name));
                    xmlVideoDriver.AppendChild(CreateElementWithText(document, "version", version));
                    xmlVideoDriversDatagroup.AppendChild(xmlVideoDriver);
                });
                xmlComputer.AppendChild(xmlVideoDriversDatagroup);
            }
            if (CollectionSettings.ANTIVIRUS) {
                SetStep($"Getting antivirus details for {computerName}");
                XmlElement xmlAntivirusDatagroup = document.CreateElement("antivirus");
                RemoteInformation.GetLatestAntivirusDefinition(computerName, (name, version) => {
                    xmlAntivirusDatagroup.AppendChild(CreateElementWithText(document, "name", name));
                    xmlAntivirusDatagroup.AppendChild(CreateElementWithText(document, "version", version));
                });
                xmlComputer.AppendChild(xmlAntivirusDatagroup);
            }
            if (CollectionSettings.LATEST_PATCH) {
                SetStep($"Getting most recent Windows patches for {computerName}");
                XmlElement xmlPatchesDatagroup = document.CreateElement("patches");
                RemoteInformation.GetMostRecentWindowsUpdates(computerName, (hotfixID, installedOn) => {
                    xmlPatchesDatagroup.AppendChild(CreateElementWithText(document, "hotfixID", hotfixID));
                    xmlPatchesDatagroup.AppendChild(CreateElementWithText(document, "installedOn", installedOn));
                });
                xmlComputer.AppendChild(xmlPatchesDatagroup);
            }
            if (CollectionSettings.INSTALLED_SOFTWARE || CollectionSettings.DRIVE_SPACE) {
                SetStep($"Getting installed software for {computerName}");
                XmlElement xmlInstalledSoftwareDatagroup = document.CreateElement("installedSoftware");
                XmlElement xmlInstalledHotfixDatagroup = document.CreateElement("deltaVHotfix");
                RemoteInformation.GetInstalledSoftware(computerName, (name, version, installedOn) => {
                    if (name.Contains("DeltaV")) {
                        if (name.Contains("WS")) {
                            XmlElement xmlInstalledHotfix = document.CreateElement("deltaVHotfix");
                            xmlInstalledHotfix.AppendChild(CreateElementWithText(document, "name", name));
                            xmlInstalledHotfix.AppendChild(CreateElementWithText(document, "installedOn", installedOn));
                            xmlInstalledHotfixDatagroup.AppendChild(xmlInstalledHotfix);
                        }
                    } else {
                        XmlElement xmlInstalledSoftware = document.CreateElement("software");
                        xmlInstalledSoftware.AppendChild(CreateElementWithText(document, "name", name));
                        xmlInstalledSoftware.AppendChild(CreateElementWithText(document, "version", version));
                        if (installedOn != null && installedOn.Trim() != "")
                            xmlInstalledSoftware.AppendChild(CreateElementWithText(document, "installedOn", installedOn));
                        xmlInstalledSoftwareDatagroup.AppendChild(xmlInstalledSoftware);
                    }
                });
                if(CollectionSettings.INSTALLED_SOFTWARE)
                    xmlComputer.AppendChild(xmlInstalledSoftwareDatagroup);
                if (CollectionSettings.DRIVE_SPACE) 
                    xmlComputer.InsertAfter(xmlInstalledHotfixDatagroup, xmlComputer.GetElementsByTagName("timezone")[0]);
            }

            if (CollectionSettings.EVENT_LOGS) {
                SetStep($"Getting Application Event logs for {computerName}");
                XmlElement xmlApplicationLogGroups = document.CreateElement("application-logs");
                RemoteInformation.GetApplicationEventLogs(computerName, (sourceName, currentDateTime, eventcode, message) => {
                    XmlElement xmlLog = document.CreateElement("log");
                    xmlLog.AppendChild(CreateElementWithText(document, "sourceName", sourceName));
                    xmlLog.AppendChild(CreateElementWithText(document, "time", currentDateTime));
                    xmlLog.AppendChild(CreateElementWithText(document, "eventcode", eventcode));
                    xmlLog.AppendChild(CreateElementWithText(document, "message", message));
                    xmlApplicationLogGroups.AppendChild(xmlLog);
                });
                xmlComputer.AppendChild(xmlApplicationLogGroups);

                SetStep($"Getting System Event logs for {computerName}");
                XmlElement xmlSystemLogGroups = document.CreateElement("system-logs");
                RemoteInformation.GetSystemEventLogs(computerName, (sourceName, currentDateTime, eventcode, message) => {
                    XmlElement xmlLog = document.CreateElement("log");
                    xmlLog.AppendChild(CreateElementWithText(document, "sourceName", sourceName));
                    xmlLog.AppendChild(CreateElementWithText(document, "time", currentDateTime));
                    xmlLog.AppendChild(CreateElementWithText(document, "eventcode", eventcode));
                    xmlLog.AppendChild(CreateElementWithText(document, "message", message));
                    xmlSystemLogGroups.AppendChild(xmlLog);
                });
                xmlComputer.AppendChild(xmlSystemLogGroups);
            }

            xmlComputer.SetAttribute("id", $"{computerName}");
            xmlCollection.AppendChild(xmlComputer);
            Logger.Log($"Collected data for {computerName}");
        }
        /// <summary>
        /// Wrapper method to create a new XMLElement and set its content
        /// </summary>
        /// <param name="document">The XMLDocument</param>
        /// <param name="name">The tag name of the element</param>
        /// <param name="content">The content of the element</param>
        /// <returns>The XMLElement</returns>
        internal XmlElement CreateElementWithText(XmlDocument document, string name, string content) {
            XmlElement xml = document.CreateElement(name);
            xml.InnerText = content;
            return xml;
        }

        internal void SetStep(string step) {
            Logger.Log(step);
            currentStepListeners.ForEach(it => it(step));
        }
        internal void CurrentStepSubscribe(Action<string> listener) {
            currentStepListeners.Add(listener);
        }
        internal void CurrentStepUnsubscribe(Action<string> listener) {
            currentStepListeners.Remove(listener);
        }
        internal void CurrentComputerSubscribe(Action<string> listener) {
            currentComputerListeners.Add(listener);
        }
        internal void CurrentComputerUnsubscribe(Action<string> listener) {
            currentComputerListeners.Remove(listener);
        }
    }
}
