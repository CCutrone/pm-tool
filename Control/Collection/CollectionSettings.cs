﻿namespace PMTool.Control.Collection {
    /// <summary>
    /// All of the collection areas that can be enabled/disabled by the user
    /// </summary>
    static class CollectionSettings {
        internal static bool SYSTEM_INFO = true;
        internal static bool DRIVE_SPACE = true;
        internal static bool NICS = true;
        internal static bool MONITORS = true;
        internal static bool VIDEO_DRIVERS = true;
        internal static bool ANTIVIRUS = true;
        internal static bool LATEST_PATCH = true;
        internal static bool INSTALLED_SOFTWARE = true;
        internal static bool EVENT_LOGS = false;
    }
}
