﻿namespace PMTool.Control.Collection {
    class Monitor {
        public string index; // The monitor index (indicates which monitor this is)
        public string posX; // The position (x) of the monitor according to the OS
        public string posY; // The position (y) of the monitor according to the OS
        public string width; // The width (in pixels) of the monitor according to the OS
        public string height; // The height (in pixels) of the monitor according to the OS
        public Monitor(string index, string posX, string posY, string width, string height) {
            this.index = index;
            this.posX = posX;
            this.posY = posY;
            this.width = width;
            this.height = height;
        }
    }
}
