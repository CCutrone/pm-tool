﻿namespace PMTool.Control.Collection {
    class NIC {
        internal string interfaceIndex; // The interface index of the NIC
        internal string name; // The name (alias) given to the NIC
        internal string status; // The status of the NIC (ie Connected, Disconnected, etc) 
        internal string[] gateways; // The list of gateways
        internal string subnet; // The subnet mask
        internal string[] ips; // The list of IPv4 and IPv6 addresses
        internal string[] dns; // The list of DNS IP addresses

        internal NIC(string interfaceIndex, string name, string status, string[] gateway, string[] subnet, string[] ips, string[] dns) {
            // if no index is assigned to the NIC, it is automatically assigned an index by the OS
            if(interfaceIndex == "-1") {
                this.interfaceIndex = "Auto";
            } else this.interfaceIndex = interfaceIndex;

            this.name = name;
            this.gateways = gateway ?? new string[] { };
            this.subnet = (subnet ?? new string[] { "" })[0];
            this.ips = ips ?? new string[] { };
            this.dns = dns ?? new string[] { };
            SetStatus(status);
        }
        /// <summary>
        /// Turns the status value from a number into a word
        /// </summary>
        /// <param name="status">The status of the NIC</param>
        internal void SetStatus(string status) {
            switch (status) {
                case "0":
                    this.status = "Disconnected";
                    break;
                case "1":
                    this.status = "Connecting";
                    break;
                case "2":
                    this.status = "Connected";
                    break;
                case "3":
                    this.status = "Disconnecting";
                    break;
                case "4":
                    this.status = "Hardware Not Present";
                    break;
                case "5":
                    this.status = "Hardware Disabled";
                    break;
                case "6":
                    this.status = "Hardware Malfunction";
                    break;
                case "7":
                    this.status = "Disconnected";
                    break;
                case "8":
                    this.status = "Authenticating";
                    break;
                case "9":
                    this.status = "Authentication Succeeded";
                    break;
                case "10":
                    this.status = "Authentication Failed";
                    break;
                case "11":
                    this.status = "Invalid Address";
                    break;
                case "12":
                    this.status = "Credentials Required";
                    break;
                default:
                    this.status = $"Unknown ({status})";
                    break;
            }
        }
    }
}
