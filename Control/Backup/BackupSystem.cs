﻿using Microsoft.VisualBasic.FileIO;
using PMTool.Helpers;
using System;
using System.Collections.Generic;
using System.IO;

namespace PMTool.Control.Backup {
    class BackupSystem {
        // The location to back files up to
        readonly static string BACKUP_LOCATION = $@"{EnvironmentHelper.DRIVE}:\ControlWorx\BACKUP_{DateTime.Today.ToString("d").Replace("/", "-")}";
        
        readonly FileCopier copier = new FileCopier();

        // Listens for updates to what step is currently being performed
        readonly List<Action<string>> currentStepListeners = new List<Action<string>>();

        // Returns the total bytes of either the files being copied or the files being compressed (depending on which one is currently being performed)
        internal long GetTotalBytes() => copier.totalBytes;

        /// <summary>
        /// Performs the file backups by copying the files to a temporary location, compressing said location, and then deleting the temporary file
        /// </summary>
        /// <returns>Any computers which were not able to be found</returns>
        internal List<string> PerformBackup() {
            List<string> computerNotFound = new List<string>();
            currentStepListeners.ForEach(it => it("Backing up files"));
            foreach (string computerName in EnvironmentHelper.Workstations) {
                try {
                    CopyFilesFromWorkstation(computerName); 
                    // copies the from each workstation, if a ComputerNotFoundException is thrown, 
                    // the computer is added to the list of computers which could not be found
                } catch (ComputerNotFoundException e) {
                    Logger.Error(e.Message);
                    computerNotFound.Add(e.Message);
                }
            }
            return computerNotFound;
        }

        /// <summary>
        /// Copies the files that need to be backed up from the specific workstation based upon if it is the ProPlus or not
        /// </summary>
        /// <param name="computerName">The name of the computer to copy the files from</param>
        internal void CopyFilesFromWorkstation(string computerName) {
            if(!Directory.Exists($@"\\{computerName}\{EnvironmentHelper.DRIVE}$")) 
                throw new ComputerNotFoundException(computerName);

            string dvdataLocation = $@"\\{computerName}\{EnvironmentHelper.DRIVE}$\DeltaV\DvData";
            string[] filesToBackup;

            if(computerName == EnvironmentHelper.PROPLUS_NAME)
                filesToBackup = new string[] { $@"{dvdataLocation}\Charts", 
                                               $@"{dvdataLocation}\Graphics-iFix", 
                                               $@"{dvdataLocation}\POWERUP",
                                               $@"{dvdataLocation}\sound" }; //If the computer is the ProPlus it needs additional files to be backed up
            else 
                filesToBackup = new string[] { $@"{dvdataLocation}\Charts" };

            copier.Copy(filesToBackup, $@"{BACKUP_LOCATION}\{computerName}");
        }

        internal void BytesTransferedSubscribe(Action<long> listener) {
            copier.ByteProgressedSubscribe(listener);
        }
        internal void BytesTransferedUnsubscribe(Action<long> listener) {
            copier.ByteProgressedUnsubscribe(listener);
        }
        internal void CurrentFileNameSubscribe(Action<string> listener) {
            copier.CurrentFileNameSubscribe(listener);
        }
        internal void CurrentFileNameUnsubscribe(Action<string> listener) {
            copier.CurrentFileNameUnsubscribe(listener);
        }
        internal void CurrentStepSubscribe(Action<string> listener) {
            copier.CurrentStepSubscribe(listener);
        }
        internal void CurrentStepUnSubscribe(Action<string> listener) {
            copier.CurrentStepUnsubscribe(listener);
        }
    }
}
