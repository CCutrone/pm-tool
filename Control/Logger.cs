﻿using PMTool.Helpers;
using System;
using System.Collections.Generic;
using System.IO;

namespace PMTool.Control {
    static class Logger {
        internal static bool DETAILED_LOGGING = false;

        private static List<string> lines = new List<string>();

        /// <summary>
        /// Logs the message only if the logging level is set to detailed logging
        /// </summary>
        /// <param name="line">The message to log</param>
        public static void Log(string line) {
            Log(line, false);
        }
        /// <summary>
        /// Logs the message only if the logging level is set to detailed logging, or if the forced boolean is true
        /// </summary>
        /// <param name="line">The message to log</param>
        public static void Log(string line, bool forced) {
            if (DETAILED_LOGGING || forced) {
                Console.WriteLine($@"[LOG] - {line}");
                lines.Add($@"[LOG] - {line}");
            }
        }
        /// <summary>
        /// Logs the warning message
        /// </summary>
        /// <param name="line">The message to log</param>
        public static void Warn(string line) {
            Console.WriteLine($@"[WARNING] - {line}");
            lines.Add($@"[WARNING] - {line}");
        }
        /// <summary>
        /// Logs the error message
        /// </summary>
        /// <param name="line">The message to log</param>
        public static void Error(string line) {
            Console.Error.WriteLine($@"[ERROR] - {line}");
            lines.Add($@"[ERROR] - {line}");
        }
        /// <summary>
        /// Saves the log to the current day's log file (appending to the already existing file if there is already a log file for the day). 
        /// Surrounding the messages with the time, and with "Process complete"
        /// </summary>
        /// <param name="line">The message to log</param>
        public static void Save() {
            string path = $@"{EnvironmentHelper.DRIVE}:\ControlWorx\Reports\Logs";
            Directory.CreateDirectory(path);
            using (StreamWriter writer = File.AppendText($@"{path}\{DateTime.Today.ToString("d").Replace("/", "-")}.txt")) {
                writer.WriteLine($"{DateTime.Today.ToLongTimeString()}");
                lines.ForEach(writer.WriteLine);
                writer.WriteLine($"Process complete\n\n");
            }
            lines.Clear();
        }
    }
}
