﻿using PMTool.Control.Collection;
using PMTool.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace PMTool.Control.Transfer {
    class TransferFiles {
        readonly FileCopier copier = new FileCopier();
        readonly List<Action<string>> currentStepListeners = new List<Action<string>>();
        readonly List<Action<string>> currentComputerListeners = new List<Action<string>>();

        internal long GetTotalBytes() => copier.totalBytes;

        /// <summary>
        /// Transfers the files from the current station to each workstation based upon its individual needs
        /// </summary>
        /// <param name="pmFolder">The path of the PM folder</param>
        internal List<string> PerformTransfer(string pmFolder) {
            if (!Directory.Exists(pmFolder)) throw new FileNotFoundException("PM folder could not be found", pmFolder);

            List<string> computerNotFound = new List<string>();

            currentStepListeners.ForEach(it => it("Retrieving patch list"));

            string patchDirectory = FileTraverser.FindChild(pmFolder, new Regex("MS Patches"));
            string wsHotfixDirectory = FileTraverser.FindChild(pmFolder, new Regex("DeltaV Hotfixes"));
            string amsHotfixDirectory = FileTraverser.FindChild(pmFolder, new Regex("^AMS.*?Updates$"));
            string wsHotfix = FileTraverser.FindChildren(wsHotfixDirectory, new Regex("_WS_"))
                                           .OrderBy((it) => it)
                                           .Last();

            foreach (string computerName in EnvironmentHelper.Workstations) {
                try {
                    PerformTransfer(computerName, pmFolder, patchDirectory, wsHotfixDirectory, amsHotfixDirectory, wsHotfix);
                } catch (ComputerNotFoundException e) {
                    Logger.Error(e.Message);
                    computerNotFound.Add(e.Message);
                }
            }
            return computerNotFound; //returns a list of any computer that was not found
        }

        private void PerformTransfer(string computerName, string pmFolder, string patchDirectory, string wsHotfixDirectory, string amsHotfixDirectory, string wsHotfix) {
            if (!Directory.Exists($@"\\{computerName}\{EnvironmentHelper.DRIVE}$"))
                throw new ComputerNotFoundException(computerName);

            currentComputerListeners.ForEach(it => it(computerName));
            currentStepListeners.ForEach(it => it($"Comparing MS patches"));


            bool hasAMS = false;
            string deltaVVersion = "";

            // In order to determine which updates are available, the bat file for that computer is read through, and already existing files are removed from the list
            RemoteInformation.GetInstalledSoftware(computerName, (name, version, installedOn) => {
                if (name == null) return;

                if (Regex.Match(name, "^AMS").Success) hasAMS = true; //if AMS is installed, the AMS patches need to be transfered
                else if (Regex.Match(name, "^DeltaV").Success) {
                    if (deltaVVersion == "" || name.Length < deltaVVersion.Length)
                        deltaVVersion = name.Trim(); //if DeltaV is installed, the DeltaV patches need to be transfered
                }
            });
            string OS = EnvironmentHelper.GetOSName(computerName);
            bool hasAMSBat = Directory.GetFiles(patchDirectory).Any(it => it.Contains("_AMS"));
            IEnumerable<string> batFiles = Directory.GetFiles(patchDirectory).Where(it => it.EndsWith(".bat") && Path.GetFileName(it).Contains(OS));

            string batFile = batFiles.First(it => it.Contains("_DeltaV"));

            if (deltaVVersion != "") batFile = batFiles.First(it => it.Contains("_" + deltaVVersion));
            else if (deltaVVersion == "" && hasAMS) batFile = batFiles.First(it => it.Contains("_AMS"));

            string[] lines = File.ReadAllLines(batFile);

            List<string> installedPatches = GetInstalledPatches(computerName);
            List<string> neededPatches = new List<string>();

            bool hasReachedScripts = false;
            string remoteDrive = $@"\\{computerName}\{pmFolder.Replace(":", "$")}_{computerName}";

            for (int i = 0; i < lines.Length; i++) {
                if (lines[i].Contains("Required Patches")) { // the list of patches begins after this line
                    hasReachedScripts = true;
                    continue;
                }
                if (!hasReachedScripts) continue;
                if (lines[i].Contains(":=")) break; // this indicates the end of the patches

                if (installedPatches.Any(it => lines[i].Contains(it))) lines[i] = "";
                else neededPatches.Add(Regex.Split(lines[i], " /").First()); // gets just the patch file and removes the command that is executed with it
            }
            currentStepListeners.ForEach(it => it($"Checking for DeltaV and AMS"));

            if (neededPatches.Count > 0) {
                string pathToMSFolder = $@"{remoteDrive}\{patchDirectory.Replace(pmFolder, "")}";
                Directory.CreateDirectory(pathToMSFolder);
                // creates a new bat file on the remote station that excludes any files that were not copied over
                File.WriteAllLines($@"{pathToMSFolder}\{Path.GetFileName(batFile)}", lines);
                // copies files from this computer to the remote station if they are a "needed" file
                copier.Copy(patchDirectory, pathToMSFolder, (name) => neededPatches.Contains(name));
            }

            if (deltaVVersion != "") {
                string pathToDeltaVFolder = $@"{remoteDrive}\{wsHotfixDirectory.Replace(pmFolder, "")}";
                Directory.CreateDirectory(pathToDeltaVFolder);
                copier.Copy(wsHotfix, pathToDeltaVFolder);
            }

            if (hasAMS) {
                string pathToAMSFolder = $@"{remoteDrive}\{amsHotfixDirectory.Replace(pmFolder, "")}";
                Directory.CreateDirectory(pathToAMSFolder);
                copier.Copy(amsHotfixDirectory, pathToAMSFolder);
            }
        }

        /// <summary>
        /// Gets the list of installed Microsoft Patches on the remote computer
        /// </summary>
        /// <param name="computerName"></param>
        /// <returns></returns>
        private static List<string> GetInstalledPatches(string computerName) {
            List<string> patches = new List<string>();
            RemoteInformation.GetWMI(computerName, "SELECT HotfixID FROM Win32_QuickFixEngineering", (obj) => {
                patches.Add(obj["HotfixID"].ToString());
            });
            return patches;
        }

        internal void ByteProgressedSubscribe(Action<long> listener) {
            copier.ByteProgressedSubscribe(listener);
        }
        internal void ByteProgressedUnsubscribe(Action<long> listener) {
            copier.ByteProgressedUnsubscribe(listener);
        }
        internal void CurrentFileNameSubscribe(Action<string> listener) {
            copier.CurrentFileNameSubscribe(listener);
        }
        internal void CurrentFileNameUnsubscribe(Action<string> listener) {
            copier.CurrentFileNameUnsubscribe(listener);
        }
        internal void CurrentStepSubscribe(Action<string> listener) {
            copier.CurrentStepSubscribe(listener);
            currentStepListeners.Add(listener);
        }
        internal void CurrentStepUnsubscribe(Action<string> listener) {
            copier.CurrentStepUnsubscribe(listener);
            currentStepListeners.Add(listener);
        }
        internal void CurrentComputerSubscribe(Action<string> listener) {
            currentComputerListeners.Add(listener);
        }
        internal void CurrentComputerUnsubscribe(Action<string> listener) {
            currentComputerListeners.Remove(listener);
        }
    }
}
