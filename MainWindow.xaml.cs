﻿using PMTool.Screens;
using System;
using System.DirectoryServices;
using System.Windows;

namespace PMTool {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            navigator.Navigate(new SelectionPage(navigator));
        }
    }
}
