﻿using PMTool.Control.Collection;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace PMTool.View {
    /// <summary>
    /// Interaction logic for CollectDataPage.xaml
    /// </summary>
    public partial class CollectDataPage : Page {
        readonly ReportGenerator reportGenerator;
        readonly Frame navigator;
        public CollectDataPage(Frame navigator) {
            InitializeComponent();
            //sets the value of all the checkboxes to the current value of the collection settings
            systemInfo.IsChecked = CollectionSettings.SYSTEM_INFO;
            driveSpace.IsChecked = CollectionSettings.DRIVE_SPACE;
            nics.IsChecked = CollectionSettings.NICS;
            monitors.IsChecked = CollectionSettings.MONITORS;
            videoDrivers.IsChecked = CollectionSettings.VIDEO_DRIVERS;
            antivirus.IsChecked = CollectionSettings.ANTIVIRUS;
            windowsPatches.IsChecked = CollectionSettings.LATEST_PATCH;
            installedSoftware.IsChecked = CollectionSettings.INSTALLED_SOFTWARE;
            eventLogs.IsChecked = CollectionSettings.EVENT_LOGS;

            this.navigator = navigator;
            reportGenerator = new ReportGenerator();
            reportGenerator.CurrentComputerSubscribe((computer) => {
                computerList.Dispatcher.BeginInvoke((Action)delegate () {
                    computerList.Text = $"{computer}\n{computerList.Text}";
                });
            });
            reportGenerator.CurrentStepSubscribe((step) => {
                currentStep.Dispatcher.BeginInvoke((Action)delegate () {
                    currentStep.Content = step;
                });
            });
        }

        private void CollectData(object sender, RoutedEventArgs e) {
            computerList.Text = "";
            backButton.IsEnabled = false;
            collectDataButton.IsEnabled = false;
            Thread bgThread = new Thread(() => {
                try {
                    List<string> failed = reportGenerator.Generate();
                    if (failed.Count != 0) { //if any computers could not be reached, inform the user
                        DisplayErrorMessage("Could not connect to some computers", string.Join("\n", failed.ToArray()));
                    }
                }
                catch (Exception exception) {
                    DisplayErrorMessage("Unexpected Exception Occured", $"{exception.Message}");
                }
                finally {
                    currentStep.Dispatcher.BeginInvoke((Action)delegate () {
                        currentStep.Content = "Finished";
                        backButton.IsEnabled = true;
                        collectDataButton.IsEnabled = true;
                    });
                }
            });
            bgThread.IsBackground = true;
            bgThread.Start();
        }
        /// <summary>
        /// Displays an error message by modifying the error message text, margin, and title
        /// </summary>
        /// <param name="title">The title of the error message</param>
        /// <param name="message">The error message</param>
        private void DisplayErrorMessage(string title, string message) {
            messagesPanel.Dispatcher.BeginInvoke((Action)delegate () {
                messagesPanel.Margin = new Thickness(0);
                messageTitle.Content = title;
                messageContent.Text = message;
            });
        }
        /// <summary>
        /// Modifies the error messages panel margins to hide it from the user
        /// </summary>
        private void DismissErrorMessages(object sender, RoutedEventArgs e) {
            messagesPanel.Margin = new Thickness(0, 0, -800, 0);
        }
        /// <summary>
        /// Modifies the settings panel margins to make it visible to the user
        /// </summary>
        private void OpenSettings(object sender, RoutedEventArgs e) {
            settingsPanel.Dispatcher.BeginInvoke((Action)delegate () {
                settingsPanel.Margin = new Thickness(0);
            });
        }
        /// <summary>
        /// Modifies the settings panel margins to hide it from the user and saves the settings
        /// </summary>
        private void DismissSettings(object sender, RoutedEventArgs e) {
            CollectionSettings.SYSTEM_INFO = systemInfo.IsChecked ?? true;
            CollectionSettings.DRIVE_SPACE = driveSpace.IsChecked ?? true;
            CollectionSettings.NICS = nics.IsChecked ?? true;
            CollectionSettings.MONITORS = monitors.IsChecked ?? true;
            CollectionSettings.VIDEO_DRIVERS = videoDrivers.IsChecked ?? true;
            CollectionSettings.ANTIVIRUS = antivirus.IsChecked ?? true;
            CollectionSettings.LATEST_PATCH = windowsPatches.IsChecked ?? true;
            CollectionSettings.INSTALLED_SOFTWARE = installedSoftware.IsChecked ?? true;
            CollectionSettings.EVENT_LOGS = eventLogs.IsChecked ?? true;
            settingsPanel.Margin = new Thickness(0, 0, -800, 0);
        }
        /// <summary>
        /// Navigates back to the selection page
        /// </summary>
        private void GoBackToSelectionPage(object sender, RoutedEventArgs e) {
            navigator.GoBack();
        }
    }
}
