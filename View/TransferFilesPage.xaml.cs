﻿using PMTool.Control.Transfer;
using PMTool.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace PMTool.View {
    /// <summary>
    /// Interaction logic for TransferFilesPage.xaml
    /// This page is used for transfering the necessary PM files to each computer in the system
    /// </summary>
    public partial class TransferFilesPage : Page {
        readonly Frame navigator;
        readonly TransferFiles transferer = new TransferFiles();
        public TransferFilesPage(Frame navigator) {
            InitializeComponent();
            // Assumes a default PM folder location using the current date
            pmFolderLocation.Text = $@"{EnvironmentHelper.DRIVE}:\ControlWorx\PM_{DateTime.Today.ToString("d").Replace("/", "-")}";
            this.navigator = navigator;
            transferer.CurrentFileNameSubscribe((name) => {
                currentFile.Dispatcher.BeginInvoke((Action)delegate () {
                    currentFile.Content = name;
                });
            });
            transferer.ByteProgressedSubscribe((bytes) => {
                progress.Dispatcher.BeginInvoke((Action)delegate () {
                    progress.Maximum = transferer.GetTotalBytes();
                    progress.Value = bytes;
                    bytesTransfered.Content = $"{bytes / 1024:#,##0}KB of {(transferer.GetTotalBytes() / 1024):#,##0}KB";
                });
            });
            transferer.CurrentStepSubscribe((step) => {
                currentStep.Dispatcher.BeginInvoke((Action)delegate () {
                    currentStep.Content = step;
                });
            });
            transferer.CurrentComputerSubscribe((computer) => {
                computerList.Dispatcher.BeginInvoke((Action)delegate () {
                    computerList.Text = $"{computer}\n{computerList.Text}";
                });
            });
        }
        /// <summary>
        /// Disables the buttons on the screen and then begins the file transfer process by providing the 
        /// TransferFiles object with the list of computers and the PM folder
        /// </summary>
        private void TransferFiles(object sender, RoutedEventArgs e) {
            backButton.IsEnabled = false;
            transferFilesButton.IsEnabled = false;
            string folder = pmFolderLocation.Text;
            Thread bgThread = new Thread(() => {
                try {
                    List<string> failed = transferer.PerformTransfer(folder);
                    if (failed.Count != 0) {
                        DisplayErrorMessage("Could not connect to some computers", string.Join("\n", failed.ToArray()));
                    }
                }
                catch (FileNotFoundException exception) {
                    DisplayErrorMessage("File not found", $"{exception.Message}\n{exception.FileName}");
                }
                finally {
                    currentStep.Dispatcher.BeginInvoke((Action)delegate () {
                        currentStep.Content = "Finished";
                        backButton.IsEnabled = true;
                        transferFilesButton.IsEnabled = true;
                    });
                }
            });
            bgThread.IsBackground = true;
            bgThread.Start();
        }
        /// <summary>
        /// Displays an error message by modifying the error message text, margin, and title
        /// </summary>
        /// <param name="title">The title of the error message</param>
        /// <param name="message">The error message</param>
        private void DisplayErrorMessage(string title, string message) {
            messagesPanel.Dispatcher.BeginInvoke((Action)delegate () {
                messagesPanel.Margin = new Thickness(0);
                messageTitle.Content = title;
                messageContent.Text = message;
            });
        }
        /// <summary>
        /// Modifies the error messages panel margins to hide it from the user
        /// </summary>
        private void DismissErrorMessages(object sender, RoutedEventArgs e) {
            messagesPanel.Margin = new Thickness(0, 0, -800, 0);
        }
        /// <summary>
        /// Navigates back to the selection page
        /// </summary>
        private void GoBackToSelectionPage(object sender, RoutedEventArgs e) {
            navigator.GoBack();
        }
        /// <summary>
        /// Modifies the settings panel margins to make it visible to the user
        /// </summary>
        private void OpenSettings(object sender, RoutedEventArgs e) {
            settingsPanel.Dispatcher.BeginInvoke((Action)delegate () {
                settingsPanel.Margin = new Thickness(0);
            });
        }
        /// <summary>
        /// Modifies the settings panel margins to hide it from the user
        /// </summary>
        private void DismissSettings(object sender, RoutedEventArgs e) {
            settingsPanel.Margin = new Thickness(0, 0, -800, 0);
        }
    }
}
