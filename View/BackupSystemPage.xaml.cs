﻿using PMTool.Control.Backup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace PMTool.Screens {
    /// <summary>
    /// Interaction logic for BackupSystemPage.xaml
    /// This page allows the user to perform system backups (ie, ProPlus folders, and the Charts folder from each station)
    /// </summary>
    public partial class BackupSystemPage : Page {
        Frame navigator;
        BackupSystem backup = new BackupSystem();
        public BackupSystemPage(Frame navigator) {
            InitializeComponent();
            this.navigator = navigator;
            backup.CurrentFileNameSubscribe((name) => {
                currentFile.Dispatcher.BeginInvoke((Action) delegate () {
                    currentFile.Content = name;
                });
            });
            backup.BytesTransferedSubscribe((bytes) => {
                progress.Dispatcher.BeginInvoke((Action)delegate () {
                    progress.Maximum = backup.GetTotalBytes();
                    progress.Value = bytes;
                    bytesTransfered.Content = $"{bytes / 1024:#,##0}KB of {(backup.GetTotalBytes() / 1024):#,##0}KB";
                });
            });
            backup.CurrentStepSubscribe((step) => {
                currentStep.Dispatcher.BeginInvoke((Action)delegate () {
                    currentStep.Content = step;
                });
            });
        }

        private void BackupSystem(object sender, RoutedEventArgs e) {
            backButton.IsEnabled = false;
            backupSystemButton.IsEnabled = false;

            Thread bgThread = new Thread(() => {
                try {
                    List<string> failed = backup.PerformBackup();
                    if (failed.Count != 0) {
                        DisplayErrorMessage("Could not connect to some computers", string.Join("\n", failed.ToArray()));
                    }
                }
                catch (FileNotFoundException exception) {
                    DisplayErrorMessage("File not found", $"{exception.Message}\n{exception.FileName}");
                }
                finally {
                    currentStep.Dispatcher.BeginInvoke((Action)delegate () {
                        currentStep.Content = "Finished";
                        backButton.IsEnabled = true;
                        backupSystemButton.IsEnabled = true;
                    });
                }
            }) {
                IsBackground = true
            };
            bgThread.Start();
        }
        /// <summary>
        /// Displays an error message by modifying the error message text, margin, and title
        /// </summary>
        /// <param name="title">The title of the error message</param>
        /// <param name="message">The error message</param>
        private void DisplayErrorMessage(string title, string message) {
            messagesPanel.Dispatcher.BeginInvoke((Action)delegate () {
                messagesPanel.Margin = new Thickness(0);
                messageTitle.Content = title;
                messageContent.Text = message;
            });
        }
        /// <summary>
        /// Modifies the error messages panel margins to hide it from the user
        /// </summary>
        private void DismissErrorMessages(object sender, RoutedEventArgs e) {
            messagesPanel.Margin = new Thickness(0, 0, -800, 0);
        }
        /// <summary>
        /// Navigates back to the selection page
        /// </summary>
        private void GoBackToSelectionPage(object sender, RoutedEventArgs e) {
            navigator.GoBack();
        }
    }
}
