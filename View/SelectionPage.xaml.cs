﻿using PMTool.Control;
using PMTool.Helpers;
using PMTool.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PMTool.Screens {
    /// <summary>
    /// Interaction logic for SelectionPage.xaml
    /// This page allows users to select which action they currently wish to be performing
    /// </summary>
    public partial class SelectionPage : Page {
        readonly Frame navigator;

        public SelectionPage(Frame navigator) {
            InitializeComponent();
            this.navigator = navigator;
            this.version.Content += EnvironmentHelper.BUILD_NUMBER;
            foreach (string computer in EnvironmentHelper.Workstations) {
                computerList.Text += $"{computer}\n";
            }
            detailedLogging.IsChecked = Logger.DETAILED_LOGGING;
        }
        /// <summary>
        /// Modifies the settings panel margins to make it visible to the user
        /// </summary>
        private void OpenSettings(object sender, RoutedEventArgs e) {
            settingsPanel.Dispatcher.BeginInvoke((Action)delegate () {
                settingsPanel.Margin = new Thickness(0);
            });
        }
        /// <summary>
        /// Modifies the settings panel margins to hide it from the user
        /// </summary>
        private void DismissSettings(object sender, RoutedEventArgs e) {
            //updates the list of workstations
            EnvironmentHelper.Workstations = computerList.Text
                                                         .Split(new char[] {',', '\n'})
                                                         .Select(it => it.Trim())
                                                         .Where(it => it != "")
                                                         .ToList();
            Logger.DETAILED_LOGGING = detailedLogging.IsChecked ?? true;
            settingsPanel.Margin = new Thickness(0, 0, -800, 0);
        }
        private void SwitchToBackupSystem(object sender, RoutedEventArgs e) {
            navigator.Navigate(new BackupSystemPage(navigator));
        }
        private void SwitchToTransferFiles(object sender, RoutedEventArgs e) {
            navigator.Navigate(new TransferFilesPage(navigator));
        }

        private void SwitchToCollectData(object sender, RoutedEventArgs e) {
            navigator.Navigate(new CollectDataPage(navigator));
        }
    }
}
