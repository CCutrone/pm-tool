﻿namespace PMTool.Helpers {
    class DeltaVNode {
        internal string name;
        internal string type;
        internal string primary;
        internal string secondary;
        internal bool redundant;
        internal DeltaVNode(string name, string type, string primary, string secondary, bool redundant) {
            this.name = name;
            this.type = type;
            this.primary = primary;
            this.secondary = secondary;
            this.redundant = redundant;
        }
    }
}
