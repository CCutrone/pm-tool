﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PMTool.Helpers {
    abstract class FileHelper {
        // Listens for updates to the amount of bytes processed
        internal readonly List<Action<long>> bytesProgressedListeners = new List<Action<long>>();
        // Listens for updates to which file is the current file
        internal readonly List<Action<string>> currentFileNameListeners = new List<Action<string>>();
        // Listens for updates to what step is currently being performed
        internal readonly List<Action<string>> currentStepListeners = new List<Action<string>>();

        // The total bytes that will need to be transfered
        public long totalBytes = 0;

        internal void ByteProgressedSubscribe(Action<long> listener) {
            bytesProgressedListeners.Add(listener);
        }
        internal void ByteProgressedUnsubscribe(Action<long> listener) {
            bytesProgressedListeners.Remove(listener);
        }
        internal void CurrentFileNameSubscribe(Action<string> listener) {
            currentFileNameListeners.Add(listener);
        }
        internal void CurrentFileNameUnsubscribe(Action<string> listener) {
            currentFileNameListeners.Remove(listener);
        }
        internal void CurrentStepSubscribe(Action<string> listener) {
            currentStepListeners.Add(listener);
        }
        internal void CurrentStepUnsubscribe(Action<string> listener) {
            currentStepListeners.Remove(listener);
        }
        /// <summary>
        /// Helper method for getting the combined size of multiple directories
        /// </summary>
        /// <param name="paths">The paths to each directory</param>
        /// <param name="filter">The (optional) filter to apply</param>
        /// <returns>The size (in bytes) of all the directories combined</returns>
        internal static long GetSizeOfDirectories(string[] paths, Func<string, bool> filter = null) {
            long totalBytes = 0;
            foreach (string path in paths)
                totalBytes += GetSizeOfDirectory(path, filter);
            
            return totalBytes;
        }
        /// <summary>
        /// Gets the size of a directory including its sub directories but 
        /// excluding anything that fails to match the filter
        /// </summary>
        /// <param name="path">The path to the directory</param>
        /// <param name="filter">The (optional) filter to apply</param>
        /// <returns>The size (in bytes) of the directory</returns>
        internal static long GetSizeOfDirectory(string path, Func<string, bool> filter = null) {
            if (!File.Exists(path) && !Directory.Exists(path)) throw new FileNotFoundException("Missing file while trying to get directory size", path);

            if (filter == null) filter = (name) => true;
            long size = 0;

            FileTraverser.Traverse(path, (fileinfo, _) => {
                if (filter(fileinfo.Name)) size += fileinfo.Length;
            }, "");
            return size;
        }
    }
}
