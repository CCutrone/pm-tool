﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Documents;

namespace PMTool.Helpers {
    static class FileTraverser {
        /// <summary>
        /// Finds the first child (file or directory) that matches the RegEx
        /// </summary>
        /// <param name="path">The path of the parent directory</param>
        /// <param name="query">The query that the child needs to match</param>
        /// <returns></returns>
        private static string FindChildRecursively(string path, Regex query) {
            string[] children = GetAllChildren(path);
            foreach (string child in children) {
                string name = Path.GetFileName(child);
                if (query.Match(name).Success) return child;
                else if (Directory.Exists(child)) { // If the item does not match the query, and it is a directory, repeat the check with all of its children
                    string response = FindChildRecursively(child, query);
                    if (response != "") return response;
                }
            }
            return "";
        }
        /// <summary>
        /// Finds the first child (file or directory) that matches the RegEx
        /// </summary>
        /// <param name="path">The path of the parent directory</param>
        /// <param name="query">The query that the child needs to match</param>
        /// <returns></returns>
        private static List<string> FindChildrenRecursively(string path, Regex query, List<string> matchedChildren) {
            string[] children = GetAllChildren(path);
            foreach (string child in children) {
                string name = Path.GetFileName(child);
                if (query.Match(name).Success) matchedChildren.Add(child);
                else if (Directory.Exists(child)) { // If the item does not match the query, and it is a directory, repeat the check with all of its children
                    FindChildrenRecursively(child, query, matchedChildren);
                }
            }
            return matchedChildren;
        }
        /// <summary>
        /// The internal facing access for the FindChildrenRecursively method, throws an exception if no child is found
        /// </summary>
        /// <param name="path">The path of the parent directory</param>
        /// <param name="query">The query that the child needs to match</param>
        /// <returns></returns>
        internal static List<string> FindChildren(string path, Regex query) {
            List<string> children = FindChildrenRecursively(path, query, new List<string>());
            if (children.Count == 0) throw new FileNotFoundException($"Could not find any children that matched the regex \"{query}\"", path);
            return children;
        }
        /// <summary>
        /// The internal facing access for the FindChildRecursively method, throws an exception if no child is found
        /// </summary>
        /// <param name="path">The path of the parent directory</param>
        /// <param name="query">The query that the child needs to match</param>
        /// <returns></returns>
        internal static string FindChild(string path, Regex query) {
            string child = FindChildRecursively(path, query);
            if(child == "") throw new FileNotFoundException($"Could not find a child that matched the regex \"{query}\"", path);
            return child;
        }
        /// <summary>
        /// Gets the path of all child elements (folder and file) and returns them as a list of strings
        /// </summary>
        /// <param name="path">The path of the parent directory</param>
        /// <returns></returns>
        internal static string[] GetAllChildren(string path) {
            if (!Directory.Exists(path)) throw new FileNotFoundException("The path either did not exist, or was not a directory", path);
            string[] directories = Directory.GetDirectories(path);
            string[] files = Directory.GetFiles(path);
            string[] children = new string[directories.Length + files.Length];
            directories.CopyTo(children, 0);
            files.CopyTo(children, directories.Length);
            return children;
        }

        /// <summary>
        /// Traverses through a directory and performs the action on each file, passing 
        /// along the fileinfo for that file as well as the difference in the original path 
        /// and the parent directory of the current file
        /// </summary>
        /// <param name="source">The current path</param>
        /// <param name="action">The action to be performed</param>
        /// <param name="traversedPath">The difference between the original path and the current path</param>
        internal static void Traverse(string source, Action<FileInfo, string> action, string traversedPath = "") {
            if (File.Exists(source)) {
                action(new FileInfo(source), traversedPath);
            }
            else if (Directory.Exists(source)) {
                string[] files = Directory.GetFiles(source);
                foreach (string file in files) {
                    string name = Path.GetFileName(file);
                    action(new FileInfo(Path.Combine(source, name)), traversedPath);
                }
                string[] directories = Directory.GetDirectories(source);
                foreach (string directory in directories) {
                    string name = Path.GetFileName(directory);
                    Traverse(Path.Combine(source, name), action, Path.Combine(traversedPath, name));
                }
            }
        }
    }
}
