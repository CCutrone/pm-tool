﻿using System;
using System.IO;

namespace PMTool.Helpers {
    class FileCopier: FileHelper {
        // Keeps track of the total size of all the transfered files
        long currentBlockSize = 0;

        /// <summary>
        /// Copies the files from the source directories to the destination directory (applies an [optional] filter to the files before transfering them).
        /// Gets the total size of the directory to be copied before begining the copy process
        /// </summary>
        /// <param name="sources">The source directories</param>
        /// <param name="destination">The destination directory</param>
        /// <param name="filter">The (optional) filter to apply</param>
        internal void Copy(string[] sources, string destination, Func<string, bool> filter = null) {
            if (filter == null) filter = (name) => true;
            currentStepListeners.ForEach(it => it("Calculating Directory size"));
            totalBytes = GetSizeOfDirectories(sources, filter);
            currentStepListeners.ForEach(it => it("Copying Files"));
            foreach (string source in sources) {
                string name = Path.GetFileName(source);
                FileTraverser.Traverse(source, (fileInfo, path) => {
                    if (filter(fileInfo.Name)) 
                        Copy(fileInfo.FullName, Path.Combine(Path.Combine(destination, path), fileInfo.Name));
                }, name);
            }
        }
        /// <summary>
        /// Copies the files from the source directory to the destination directory (applies an [optional] filter to the files before transfering them).
        /// Gets the total size of the directory to be copied before begining the copy process
        /// </summary>
        /// <param name="source">The source directory</param>
        /// <param name="destination">The destination directory</param>
        /// <param name="filter">The (optional) filter to apply</param>
        internal void Copy(string source, string destination, Func<string, bool> filter = null) {
            if (filter == null) filter = (name) => true;
            currentStepListeners.ForEach(it => it("Calculating Directory size"));
            totalBytes = GetSizeOfDirectory(source, filter);
            currentStepListeners.ForEach(it => it("Copying Files"));
            FileTraverser.Traverse(source, (fileInfo, path) => { //Traverses to the 
                if(filter(fileInfo.Name))
                    Copy(fileInfo.FullName, Path.Combine(Path.Combine(destination, path), fileInfo.Name));
            }, "");
        }
        /// <summary>
        /// Copies a file from the source directory to the destination directory
        /// Makes calls to each byte progress and current file name listener that has subscribed
        /// </summary>
        /// <param name="source">The source directory</param>
        /// <param name="destination">The destination directory</param>
        private void Copy(string source, string destination){
            byte[] buffer = new byte[1024 * 1024];
            int temp = 0;
            using (FileStream sourceStream = new FileStream(source, FileMode.Open, FileAccess.Read)) {
                currentFileNameListeners.ForEach(it => it(Path.GetFileName(destination))); // Updates the listeners with the current file name
                if (!File.Exists(destination)) {
                    Directory.CreateDirectory(Path.GetDirectoryName(destination)); // Creates the destination directory if it does not exist
                    using (FileStream destinationStream = new FileStream(destination, FileMode.CreateNew, FileAccess.Write)) {
                        while ((temp = sourceStream.Read(buffer, 0, buffer.Length)) > 0) {
                            // for as long as there are untransfered bytes, transfer them, update the size of the total bytes transfered, and update the byte progress listeners
                            destinationStream.Write(buffer, 0, temp);
                            currentBlockSize += temp;
                            bytesProgressedListeners.ForEach(it => it(currentBlockSize));
                        }
                    }
                } else {
                    // If the file already exists in the destination just skip it and update the current block size as well as all the listeners
                    currentBlockSize += new FileInfo(destination).Length;
                    bytesProgressedListeners.ForEach(it => it(currentBlockSize));
                }
            }
        }
    }
}
