﻿using PMTool.Control;
using PMTool.Control.Collection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace PMTool.Helpers {
    static class EnvironmentHelper {

        static EnvironmentHelper() {
            RemoteInformation.GetRegistryValueObjects(Environment.MachineName, new string[] { @"SOFTWARE\Wow6432Node\FRSI\DeltaV\CurrentVersion", @"SOFTWARE\FRSI\DeltaV\CurrentVersion" }, (subKey) => {
                if (subKey.GetValue("DataPath") is string tempDvDataLocation && tempDvDataLocation.Trim() != "") DV_DATA_LOCATION = tempDvDataLocation;
            });
            Logger.Log(IS_DEVELOPMENT_ENVIRONMENT ? "Using development environment": "Using production environment", true);
        }

        // <Major>.<Minor>.<Revision>.<Bugfix>
        // Major is for major changes
        // Minor is for new features
        // Revision is for updates to the blacklist/configuration settings
        // Bugfix is for bugfixes
        public readonly static string BUILD_NUMBER = "1.3.1.0";
        public static string DV_DATA_LOCATION = "";
        public readonly static bool IS_DEVELOPMENT_ENVIRONMENT = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("JHC_DEV"));
        public readonly static string DRIVE = IS_DEVELOPMENT_ENVIRONMENT ? "C" : "D";
        public readonly static string PROPLUS_IP = IS_DEVELOPMENT_ENVIRONMENT ? "127.0.0.1" : "10.4.0.6";
        public static string PROPLUS_NAME {
            get {
                try {
                    return Dns.GetHostEntry(PROPLUS_IP).HostName.ToString().Split('.').First();
                } catch (Exception) {
                    return Dns.GetHostEntry("127.0.0.1").HostName.ToString().Split('.').First();
                }
            }
        }
        public static List<string> Workstations = GetListOfDeltaVWorkstations().ToList();
        public static List<DeltaVNode> DeltaVNodes = GetListOfDeltaVNodes().OrderBy(it => it.name).ToList();

        /// <summary>
        /// Reads through the DT.SCR file from DeltaV to get a list of DeltaV workstations
        /// </summary>
        /// <returns>The list of workstations</returns>
        public static string[] GetListOfDeltaVWorkstations() {
            if (!File.Exists($@"{DV_DATA_LOCATION}\download\DT.SCR"))
                return new string[] { Environment.MachineName }; //If there is no DT.SCR file on the computer, just return the current computer's name

            //finds all the lines that inlcude TYPE=WS (indictating that the entry is not a controller, or CIOC but is in fact a workstation)
            //Takes those lines and splits them at the occurance of "NAM=" , indicating that the text to follow is the name.
            return File.ReadAllText($@"{DV_DATA_LOCATION}\download\DT.SCR")
                        .Split('\n')
                        .Where(line => line.StartsWith("DTI") && Regex.Match(line, "TYPE=WS ").Success)
                        .Select(line =>
                            line.Split(new string[] { "NAM=" }, StringSplitOptions.None)[1]
                                .Split('\'')[1])
                        .ToArray();
        }



        /// <summary>
        /// Reads through the DT.SCR file from DeltaV to get a list of DeltaV workstations
        /// </summary>
        /// <returns>The list of workstations</returns>
        public static List<DeltaVNode> GetListOfDeltaVNodes() {
            if (!File.Exists(IS_DEVELOPMENT_ENVIRONMENT ? @"C:\DT.SCR" : $@"{DV_DATA_LOCATION}\download\DT.SCR"))
                return new List<DeltaVNode>(); //If there is no DT.SCR file on the computer, just return any empty list

            //finds all the lines that inlcude TYPE=WS (indictating that the entry is not a controller, or CIOC but is in fact a workstation)
            //Takes those lines and splits them at the occurance of "NAM=" , indicating that the text to follow is the name.
            return File.ReadAllText(IS_DEVELOPMENT_ENVIRONMENT ? @"C:\DT.SCR" :$@"{DV_DATA_LOCATION}\download\DT.SCR")
                        .Split('\n')
                        .Where(line => line.StartsWith("DTI"))
                        .Select(line => {
                            string name = line.Split(new string[] { "NAM=" }, StringSplitOptions.None)[1].Split('\'')[1];
                            string type = line.Split(new string[] { "TYPE=" }, StringSplitOptions.None)[1].Split(' ')[0];
                            string primary = line.Split(new string[] { "PRI=" }, StringSplitOptions.None)[1].Split('\'')[1];
                            string secondary = line.Split(new string[] { "SEC=" }, StringSplitOptions.None)[1].Split('\'')[1];
                            return new DeltaVNode(name, type, primary, secondary, true);
                        })
                        .ToList();
        }
        /// <summary>
        /// Uses the Win32_OperatingSystem WMI object to get the OS name, and trims it down to match the Windows [Version Number] format
        /// </summary>
        /// <param name="computerName">The name of the computer</param>
        /// <returns>The formatted OS name</returns>
        public static string GetOSName(string computerName) {
            string OS = "";
            RemoteInformation.GetWMI(computerName, "SELECT Caption FROM Win32_OperatingSystem", (result) => {
                OS = Regex.Split(result["Caption"]?.ToString()?.Replace("Microsoft ", ""), "(?<=[0-9]+)(\\s+)")[0];
            });
            return OS;
        }
    }
}
